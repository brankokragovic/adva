<?php

namespace App\Http\Controllers;

use App\Category;
use App\Company;
use App\Product;
use Illuminate\Http\Request;
use Matchish\ScoutElasticSearch\Mixed;

class SearchController extends Controller
{
    public function search(){


	$e = 	Mixed::search('name:Lorem or to:lorem')->
		within(implode(',', [
		(new Category())->searchableAs(),
		(new Company())->searchableAs(),
		(new Product())->searchableAs(),
	]))->get();

	return $e;
	}
}
