<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Company', function (Blueprint $table) {
			$table->id();
			$table->text("name");
			$table->text("description");
			$table->boolean("claimed")->default(false);
			$table->text("claimedBy");
			$table->enum("subscription", ['FREE', 'BASIC', 'ADVANCED', 'PRO'])->default('FREE');
			$table->text("website");
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('company');
	}
}
