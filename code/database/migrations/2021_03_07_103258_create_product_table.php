<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Product', function (Blueprint $table) {
            $table->id();
			$table->text('name');
			$table->text('description');
			$table->json('images');
			$table->integer('categoryId');
			$table->integer('companyId');
			$table->foreign('categoryId')
				->references('id')
				->on('Category')
				->onDelete('cascade');
			$table->foreign('companyId')
				->references('id')
				->on('Company')
				->onDelete('cascade');

			$table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
