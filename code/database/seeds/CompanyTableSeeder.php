<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker::create();
		foreach (range(1,100) as $index) {
			DB::table('Company')->insert([
				'name' => $faker->name,
				'description' => $faker->text,
				'claimed'=>  false,
				'claimedBy'=> $faker->text,
				'subscription'=> 'FREE',
				'website' => $faker->url
			]);
		}

    }

}
