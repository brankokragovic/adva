<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
	 *
     */



    public function run()
    {


		$faker = Faker::create();
		foreach (range(1,100) as $index) {
			DB::table('Product')->insert([
				'id' => $index,
				'name' => $faker->name,
				'description' => $faker->text,
				'images' => json_encode('text'),
				'categoryId' => $faker->numberBetween(1,6),
				'companyId' => $faker->numberBetween(1,6)

			]);
		}
    }
}
