
FROM php:7.2-fpm

RUN apt-get update && \
   apt-get install -y \
       zlib1g-dev


RUN apt-get update && apt-get install -y \
       wget \
       build-essential \
       libmp3lame-dev \
       libvorbis-dev \
       libtheora-dev \
       libspeex-dev \
       yasm \
       pkg-config \
       libx264-dev \
       libfreetype6 \
       libfreetype6-dev \
       libfribidi-dev \
       libfontconfig1-dev \
       libcurl4-openssl-dev \
       libssl-dev \
       libxrender1 \
       libxext6 \
       pkg-config \
       software-properties-common


RUN apt-get update && apt-get install -y \
   && docker-php-ext-install gd \
   && docker-php-ext-install mbstring \
   && docker-php-ext-enable gd


RUN pecl install redis-3.1.4 \
   && docker-php-ext-enable redis
RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update

RUN apt-get install -y libpq-dev \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql
RUN docker-php-ext-install pdo pdo_pgsql

RUN apt-get install -y zlib1g-dev && rm -rf /var/lib/apt/lists/* && docker-php-ext-install zip

RUN apt update && apt install curl && \
   curl -sS https://getcomposer.org/installer | php \
   && chmod +x composer.phar && mv composer.phar /usr/local/bin/composer